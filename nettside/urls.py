from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^timeplan', views.timeplan, name='timeplan'),
    url(r'^stilarter$', views.stilarter, name='stilarter'),
    url(r'^pedagoger$', views.pedagoger, name='pedagoger'),
    url(r'^priser$', views.priser, name='priser'),
    url(r'^forestillinger$', views.forestillinger, name='forestillinger'),
    url(r'^kontaktoss$', views.kontaktoss, name='kontaktoss'),

    url(r'^sal1$', views.sal1, name='sal1'),
    url(r'^sal2$', views.sal2, name='sal2'),

    url(r'^stilarter/barnedans$', views.barnedans, name='barnedans'),
    url(r'^stilarter/breakdance$', views.breakdance, name='breakdance'),
    url(r'^stilarter/hiphop$', views.hiphop, name='hiphop'),
    url(r'^stilarter/jazz$', views.jazz, name='jazz'),
    url(r'^stilarter/klassisk$', views.klassisk, name='klassisk'),
    url(r'^stilarter/moderne$', views.moderne, name='moderne'),
    url(r'^stilarter/taaspiss$', views.taaspiss, name='taaspiss'),
    url(r'^stilarter/vogue$', views.vogue, name='vogue'),

    url(r'^pedagoger/amelia$', views.amelia, name='amelia'),
    url(r'^pedagoger/andri$', views.andri, name='andri'),
    url(r'^pedagoger/daniel$', views.daniel, name='daniel'),
    url(r'^pedagoger/elita$', views.elita, name='elita'),
    url(r'^pedagoger/ingrid$', views.ingrid, name='ingrid'),
    url(r'^pedagoger/irlin$', views.irlin, name='irlin'),
    url(r'^pedagoger/katrine$', views.katrine, name='katrine'),
    url(r'^pedagoger/mari$', views.mari, name='mari'),
    url(r'^pedagoger/maria$', views.maria, name='maria'),
    url(r'^pedagoger/maylinda$', views.maylinda, name='maylinda'),
    url(r'^pedagoger/siljeolava$', views.siljeolava, name='siljeolava')
]
