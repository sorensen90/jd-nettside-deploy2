from django.shortcuts import render
from nettside.models import Forestillinger

def index(request):
    return render(request, 'nettside/index.html', {})

def timeplan(request):
    return render(request, 'nettside/timeplan.html', {})

def pedagoger(request):
    return render(request, 'nettside/pedagoger.html', {})

def stilarter(request):
    return render(request, 'nettside/stilarter.html', {})

def priser(request):
    return render(request, 'nettside/priser.html', {})

def forestillinger(request):
    data = Forestillinger.objects.all()
    return render(request, 'nettside/forestillinger.html', {"data":data})

def kontaktoss(request):
    return render(request, 'nettside/kontaktoss.html', {})

###############################################################################
### Saler ###
def sal1(request):
    return render(request, 'nettside/sal1.html', {})

def sal2(request):
    return render(request, 'nettside/sal2.html', {})


###############################################################################
### Stilarter ###
def barnedans(request):
    return render(request, 'nettside/stilarter/barnedans.html', {})

def breakdance(request):
    return render(request, 'nettside/stilarter/breakdance.html', {})

def hiphop(request):
    return render(request, 'nettside/stilarter/hiphop.html', {})

def jazz(request):
    return render(request, 'nettside/stilarter/jazz.html', {})

def klassisk(request):
    return render(request, 'nettside/stilarter/klassisk.html', {})

def moderne(request):
    return render(request, 'nettside/stilarter/moderne.html', {})

def taaspiss(request):
    return render(request, 'nettside/stilarter/taaspiss.html', {})

def vogue(request):
    return render(request, 'nettside/stilarter/vogue.html', {})

###############################################################################
### Pedagoger ###

def amelia(request):
    return render(request, 'nettside/pedagoger/amelia.html', {})
def andri(request):
    return render(request, 'nettside/pedagoger/andri.html', {})
def daniel(request):
    return render(request, 'nettside/pedagoger/daniel.html', {})
def elita(request):
    return render(request, 'nettside/pedagoger/elita.html', {})


def ingrid(request):
    return render(request, 'nettside/pedagoger/ingrid.html', {})
def irlin(request):
    return render(request, 'nettside/pedagoger/irlin.html', {})
def katrine(request):
    return render(request, 'nettside/pedagoger/katrine.html', {})
def mari(request):
    return render(request, 'nettside/pedagoger/mari.html', {})

def maria(request):
    return render(request, 'nettside/pedagoger/maria.html', {})
def maylinda(request):
    return render(request, 'nettside/pedagoger/maylinda.html', {})
def siljeolava(request):
    return render(request, 'nettside/pedagoger/siljeolava.html', {})



