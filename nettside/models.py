from django.db import models
from django.utils import timezone

"""
class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
"""
class Forestillinger(models.Model):
    Pedagog = models.CharField(max_length=20)
    Laat = models.CharField(max_length=120)
    Kurs = models.CharField(max_length=40)
    KostymeHjem = models.CharField(max_length=400)
    KostymeSkole = models.CharField(max_length=400)
    Inngang = models.CharField(max_length=100)
    Utgang = models.CharField(max_length=100)
    
    # This function returns the value of the row Kurs in the table Forestillinger
    # to the admin app to use as a title in the overview section for the 
    # posts. (min kommentar)
    def __str__(self):
        return self.Kurs + 'R' + self.Pedagog
